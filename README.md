**Technical Test**

*The company name is omitted on purpose.*

**Overview**

This project is a technical test.

For more information see the ReadMe.txt file in the \Documentation folder.

**Abstract**

A bookstore wants to develop a new computer tool to:
•	Import information from its stock 
•	Get information about a particular book 
•	Calculate the price of a basket of books 
You are in charge of the development of this tool whose functionalities are detailed below. You are requested to implement the different methods exposed by the interface described in Appendix 4, following the specifications below.

**Author** 

Svetoslav Mitov